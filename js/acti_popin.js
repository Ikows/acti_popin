(function($, Drupal, drupalSettings) {

    /**
     * Moment of appearence for popins.
     */
    Drupal.behaviors.actiPopinPop = {
      attach: function (context, settings) {
        setTimeout(function () {
          if (!actiPopinGetCookie("economie_popin_" + drupalSettings.popinId)) {
            $('#block-actipopinblock').once().toggleClass('actiPoped');
            if (drupalSettings.fondGris == 1) {
              $('.page').once().append('<div class="actiPopinModal"></div>');
            }
            if (drupalSettings.modalClose == 1) {
              $('.actiPopinModal').css('cursor', 'pointer');
            }
          }
        }, (parseInt(drupalSettings.timeToPop) * 1000));
      }
    };

    /**
     * Style and behavior for popins.
     */
    Drupal.behaviors.actiPopinModal = {
      attach: function (context, settings) {
        $('#popin-close-btn').click(function () {          
          $('.actiPopinModal').hide()
          $('#block-actipopinblock').hide()
          actiPopinSetCookie();
        })

        $(document).on('click', '.actiPopinModal', function () {
          if (drupalSettings.modalClose == 1) {
            $('.actiPopinModal').hide()
            $('#block-actipopinblock').hide()
            actiPopinSetCookie();
          }
        })

      }
    };

  /**
   * Set a cookie by with data.
   */
  function actiPopinSetCookie() {
    let dt = new Date();
    dt.setTime(dt.getTime() + 1000 * 3600 * 24 * parseInt(drupalSettings.popinDelay))
    document.cookie = "economie_popin_" + drupalSettings.popinId + "=disable_popin_economie; expires="+ dt.toUTCString() +"; path=/;"  
  }

  /**
   * Get a cookie by name.
   */
  function actiPopinGetCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
  })(jQuery, Drupal, drupalSettings);