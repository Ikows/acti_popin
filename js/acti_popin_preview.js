(function($, Drupal, drupalSettings) {
    
  /**
   * Preview for popins.
   */
  Drupal.behaviors.actiPopinMPreview = {
    attach: function (context, settings) {
      popinChangeColor('#ActiPopinPreviewBlock', '#edit-field-acti-popin-couleur-bordure-0-color', '#edit-field-acti-popin-couleur-bordure-0-opacity');
      popinChangeColor('#actiPopinPreviewIframe', '#edit-field-acti-popin-iframe-color-0-color', '#edit-field-acti-popin-iframe-color-0-opacity');
      popinChangeColor('#actiPopinPreviewText', '#edit-field-acti-popin-couleur-de-fond-0-color', '#edit-field-acti-popin-couleur-de-fond-0-opacity');
      $('#actiPopinPreviewText').css('color', $('#edit-field-acti-popin-color-txt-0-color').val());
      if ($('#edit-field-acti-popin-iframe-0-value').val().length < 1) {
          $('#actiPopinPreviewIframe').hide()
      }
      else {
        $('#actiPopinPreviewIframe').show()
        $('#actiPopinPreviewIframe').attr('src' , $('#edit-field-acti-popin-iframe-0-value').val())
      }

      $('#actiPopinPreviewText').html($('#edit-field-acti-popin-texte-principal-0-value').val());

      // If size of popin small
      if ($("#edit-field-acti-popin-taille-sm").is(":checked")) {
        // Set width to 400x
        $('#ActiPopinPreviewBlock').css('width', '400px');
      }
      else {
        // Otherwise Set width to 530px
        $('#ActiPopinPreviewBlock').css('width', '530px');
      }





      // Couleur bordure Popin.
      $('#color-field-field-acti-popin-couleur-bordure input').change(function () {
        let valToRound = $('#edit-field-acti-popin-couleur-bordure-0-opacity').val();
        $('#edit-field-acti-popin-couleur-bordure-0-opacity').val(Number.parseFloat(valToRound).toPrecision(2))
        popinChangeColor('#ActiPopinPreviewBlock', '#edit-field-acti-popin-couleur-bordure-0-color', '#edit-field-acti-popin-couleur-bordure-0-opacity');
        
      })
      // Couleur fond iframe.
      $('#color-field-field-acti-popin-iframe-color input').change(function () {
        popinChangeColor('#actiPopinPreviewIframe', '#edit-field-acti-popin-iframe-color-0-color', '#edit-field-acti-popin-iframe-color-0-opacity');
      })
      // Couleur fond bloc de texte principal.
      $('#color-field-field-acti-popin-couleur-de-fond input').change(function () {
        popinChangeColor('#actiPopinPreviewText', '#edit-field-acti-popin-couleur-de-fond-0-color', '#edit-field-acti-popin-couleur-de-fond-0-opacity');
      })
      // Couleur du texte principal.
      $('#color-field-field-acti-popin-color-txt input').change(function () {
        $('#actiPopinPreviewText').css('color', $('#edit-field-acti-popin-color-txt-0-color').val());
      })
      // Iframe.
      $('#edit-field-acti-popin-iframe-0-value').keyup(function () {
        if ($(this).val().length < 1) {
          $('#actiPopinPreviewIframe').hide()
        }
        else {
          $('#actiPopinPreviewIframe').show()
          $('#actiPopinPreviewIframe').attr('src' , $(this).val())
        }
      })
      // Texte du ckeditor.
      $('#popinPreviewRefresh').click(function (e) {
        e.preventDefault();
        $('.cke_button__source').click();
        $('#actiPopinPreviewText').html($('#cke_1_contents textarea').val());
        setTimeout(function () {
          $('.cke_button__source').click();
        }, 100)
      })
      // Taille de la popin.
      $('#edit-field-acti-popin-taille input').change(function () {
        if ($(this).attr('id') == "edit-field-acti-popin-taille-sm") {
          $('#ActiPopinPreviewBlock').css('width', '400px')
        }
        else {
          $('#ActiPopinPreviewBlock').css('width', '530px')
        }
      })

      function popinChangeColor(toChange, color, opacity) {
        let rgbObj = hexToRgb($(color).val());
        let rgbaColor = 'rgba(' + rgbObj.r + ', ' + rgbObj.g + ', ' + rgbObj.b + ', ' + $(opacity).val() + ')';
        $(toChange).css('background-color', rgbaColor);
      }
    }
  };

  /**
   * Hex to rbg function.
   */
  function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }
  
  })(jQuery, Drupal, drupalSettings);