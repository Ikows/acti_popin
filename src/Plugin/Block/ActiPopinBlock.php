<?php

namespace Drupal\acti_popin\Plugin\Block;

use Drupal\node\Entity\Node;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ActiPopinBlock' block.
 *
 * @Block(
 *  id = "acti_popin_block",
 *  admin_label = @Translation("Acti popin block"),
 * )
 */
class ActiPopinBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['acti_popin_block']['#cache'] = [
      'contexts' => ['url.path', 'url.query_args'],
      'tags' => ['block:acti_popin_block'],
    ];

    // On teste d'abord le cas d'unr provenance de newsletter.
    if ($xtor = \Drupal::request()->query->get('xtor')) {
      $database = \Drupal::database();
      $query = $database->select('node_field_data', 'n');
      $query->condition('n.status', 1);
      $query->join('node__field_acti_popin_prov', 'pp', 'pp.entity_id = n.nid');
      $query->fields('n', ['nid']);
      $query->fields('pp', ['field_acti_popin_prov_value']);
      $results = $query->execute()->fetchAllKeyed();
      foreach ($results as $key => $result) {
        if (trim($result) == $xtor) {
          $nids[] = $key;
        }
      }
    }
    else {
      // Init des variables et query.
      $database = \Drupal::database();
      $query = $database->select('node_field_data', 'n');
      $query->join('node__field_acti_popin_inclusion', 'pi', 'pi.entity_id = n.nid');
      $query->condition('n.status', 1);
      $query->fields('n', ['nid']);
      $query->fields('pi', ['field_acti_popin_inclusion_value']);
      $result = $query->execute()->fetchAllKeyed();
      $pathInfo = \Drupal::request()->getPathInfo();

      // Etablissement des patterns éligibles.
      if ($pathInfo != '/') {
        $availablePatterns[] = '/*';
      }
      $availablePatterns[] = $pathInfo;
      $pathArray = explode('/', $pathInfo);
      $url = '';
      foreach ($pathArray as $key => $value) {
        if ($key != 0 && $key != count($pathArray) - 1) {
          $url = $url . '/' . $value;
          $availablePatterns[] = $url . '/*';
        }
      }

      // On cherche si un résultat match avec le pattern.
      $nids = [];
      foreach ($result as $key => $value) {
        $xpldValue = explode("\r\n", $value);
        if (array_intersect($xpldValue, $availablePatterns)) {
          $nids[] = $key;
        }
      }
    }

    // Si ça match, on cherche si le ou les noeuds ne sont pas exclus.
    $node = FALSE;
    if ($nids) {
      $nodes = Node::loadMultiple($nids);
      foreach ($nodes as $key => $value) {
        $xpldValue = explode("\r\n", $value->get('field_acti_popin_exclusion')->value);
        if (array_intersect($xpldValue, $availablePatterns)) {
          unset($nodes[$key]);
        }
      }
      $node = reset($nodes);
    }
    // On prépare les infos de la popin pour le template.
    if ($node) {
      $build['acti_popin_block'] += [
        '#theme' => 'acti_popin',
        '#iframe' => $node->field_acti_popin_iframe->value,
        '#main_text' => $node->field_acti_popin_texte_principal->view('full'),
        '#acti_border_color' => $this->hexToRgba($node->field_acti_popin_couleur_bordure->color, $node->field_acti_popin_couleur_bordure->opacity),
        '#acti_txt_color' => $this->hexToRgba($node->field_acti_popin_color_txt->color, $node->field_acti_popin_color_txt->opacity),
        '#acti_iframe_color' => $this->hexToRgba($node->field_acti_popin_iframe_color->color, $node->field_acti_popin_iframe_color->opacity),
        '#acti_couleur_fond' => $this->hexToRgba($node->field_acti_popin_couleur_de_fond->color, $node->field_acti_popin_couleur_de_fond->opacity),
      ];

      $build['#attributes']['class'][] = 'popin-' . $node->field_acti_popin_taille->value;
      $build['#attributes']['class'][] = 'popin-h-' . $node->field_acti_popin_horizontal->value;
      $build['#attributes']['class'][] = 'popin-v-' . $node->field_acti_popin_vertical->value;
      foreach ($node->field_acti_popin_devices->getValue() as $key => $device) {
        $build['#attributes']['class'][] = 'popin-device-' . $device['value'];
      }

      $build['acti_popin_block']['#attached'] = [
        'library' => [
          'acti_popin/iframe-resizer',
          'acti_popin/custom',
          'acti_popin/iframe-resizer-contentWindow',
          'acti_popin/acti_popin',
        ],
        'drupalSettings' => [
          'timeToPop' => $node->field_acti_popin_mom_affichage->value,
          'fondGris' => $node->field_acti_popin_fond_gris->value,
          'modalClose' => $node->field_acti_popin_close->value,
          'popinDelay' => $node->field_acti_popin_delay->value,
          'popinId' => $node->id(),
        ],
      ];
    }
    return $build;

  }

  /**
   * Convert hexa color and opacity to rgba.
   *
   * @param string $color
   *   The color of the field.
   * @param string $opacity
   *   The opacity of the field.
   *
   * @return string
   *   The rgba version of hexa and opacity.
   */
  private function hexToRgba(string $color, string $opacity) : string {
    list($r, $g, $b) = sscanf($color, "#%02x%02x%02x");
    return 'rgba(' . $r . ', ' . $g . ', ' . $b . ', ' . $opacity . ')';
  }

}
